import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Route from './Route';
import Login from '../pages/Login';
import Home from '../pages/Home';

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route exact path="/" component={Home} isPrivate />
    </Switch>
  </Router>
);

export default Routes;
