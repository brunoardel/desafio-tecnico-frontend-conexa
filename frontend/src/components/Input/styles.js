import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles(() => ({
  'required-label': {
    '& > span': {
      color: '#f44336',
    },
  },
}));
