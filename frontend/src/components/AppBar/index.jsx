import React from 'react';
import PropTypes from 'prop-types';
import AppBarMui from '@material-ui/core/AppBar';
import Hidden from '@material-ui/core/Hidden';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import { useAuth } from '../../hooks/auth';
import Button from '@material-ui/core/Button';
import logoConexa from '../../assets/images/logo-conexa.svg';
import { useStyles } from './styles';

function AppBar() {
  const classes = useStyles();
  const { pathname } = useLocation();
  const isHome = pathname === '/' && true;
  const { name: user, signOut } = useAuth();

  return (
    <div className={classes.root}>
      <AppBarMui position="sticky" color="inherit" elevation={1}>
        <Toolbar className={classes.toolbar}>
          <img
            src={logoConexa}
            alt="logo conexa"
            to={'/'}
            component={RouterLink}
            className={classes.logo}
          />
          <div style={{ flexGrow: 1 }}></div>
          {isHome && (
            <div className={classes.appbarGreetingLogout}>
              <Hidden smDown>
                <Typography
                  variant="h6"
                  align="left"
                  color="textPrimary"
                  className={classes.appbarGreeting}
                >
                  Olá, Dr. {user}
                </Typography>
              </Hidden>
              <Button
                variant="outlined"
                color="primary"
                edge="end"
                onClick={signOut}
              >
                Sair
              </Button>
            </div>
          )}
        </Toolbar>
      </AppBarMui>
      <div />
    </div>
  );
}

AppBar.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.instanceOf(
    typeof Element === 'undefined' ? Object : Element,
  ),
};

export default AppBar;
