import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  logo: {
    color: 'white',
    textDecoration: 'none',

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0),
      margin: '0 auto',
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: '100%',
      backgroundColor: '#FFFFFB',
      boxShadow: '4px 4px 12px rgba(0, 0, 0, 0.05)',
      display: 'flex',
      flexDirection: 'column',
    },
  },
  active: {
    backgroundColor: theme.palette.action.selected,
  },
  appbarGreetingLogout: {
    display: 'flex',
  },
  appbarGreeting: {
    marginRight: theme.spacing(3),
  },
}));
