import React, { createContext, useState, useContext } from 'react';

const ConsultationsContext = createContext();

const ConsultationsProvider = ({ children }) => {
  const [consultations, setConsultations] = useState([]);

  return (
    <ConsultationsContext.Provider value={{ consultations, setConsultations }}>
      {children}
    </ConsultationsContext.Provider>
  );
};

function useConsultations() {
  const context = useContext(ConsultationsContext);

  return context;
}

export { ConsultationsProvider, useConsultations };
