import React from 'react';

import { LoaderProvider } from './loader';
import { AuthProvider } from './auth';
import { ConsultationsProvider } from './consultations';

const AppProvider = ({ children }) => (
  <LoaderProvider>
    <AuthProvider>
      <ConsultationsProvider>{children}</ConsultationsProvider>
    </AuthProvider>
  </LoaderProvider>
);
export default AppProvider;
