import React, { createContext, useCallback, useState, useContext } from 'react';
import api from '../services/api';

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  const [data, setData] = useState(() => {
    const name = localStorage.getItem('Conexa:Name');
    const token = localStorage.getItem('Conexa:Token');

    if (name && token) {
      return { name: JSON.parse(name), token };
    }

    return {};
  });

  const signIn = useCallback(async ({ email, password }) => {
    const response = await api.post('/login', { email, password });

    const { name, token } = response.data;

    localStorage.setItem('Conexa:Name', JSON.stringify(name));
    localStorage.setItem('Conexa:Token', token);

    setData({ name, token });
  }, []);

  const signOut = useCallback(() => {
    localStorage.removeItem('Conexa:Name');
    localStorage.removeItem('Conexa:Token');

    setData({});
  }, []);

  return (
    <AuthContext.Provider value={{ name: data.name, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth() {
  const context = useContext(AuthContext);

  return context;
}

export { AuthProvider, useAuth };
