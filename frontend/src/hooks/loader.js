import React, { createContext, useContext, useState } from 'react';
import Loader from '../components/Loader';

const LoaderContext = createContext();

const LoaderProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);

  return (
    <LoaderContext.Provider value={{ loading, setLoading }}>
      {children}
      {loading && <Loader />}
    </LoaderContext.Provider>
  );
};

function useLoader() {
  const context = useContext(LoaderContext);

  return context;
}

export { LoaderProvider, useLoader };
