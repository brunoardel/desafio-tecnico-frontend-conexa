import AppProvider from './hooks';
import CssBaseline from '@material-ui/core/CssBaseline';
import Routes from './routes';

function App() {
  return (
    <main className="App">
      <AppProvider>
        <CssBaseline />
        <Routes />
      </AppProvider>
    </main>
  );
}

export default App;
