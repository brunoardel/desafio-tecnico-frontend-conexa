import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function FormDialog({ open, onClose }) {
  return (
    <div>
      <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Agendar consulta</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Para agendar uma nova consulta, entre com os dados nos campos
            abaixo.
          </DialogContentText>
          <form onSubmit="">
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Nome completo"
              type="text"
              fullWidth
              required
              InputLabelProps={{ shrink: true }}
            />
            <TextField
              margin="dense"
              id="date"
              label="Data"
              type="date"
              fullWidth
              required
              InputLabelProps={{ shrink: true }}
            />
            <TextField
              margin="dense"
              id="time"
              label="Hora"
              type="time"
              fullWidth
              required
              InputLabelProps={{ shrink: true }}
            />
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={onClose} color="primary">
            Agendar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
