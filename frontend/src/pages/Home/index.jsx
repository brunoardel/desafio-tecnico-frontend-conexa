import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import AppBar from '@material-ui/core/AppBar';
import TopAppBar from '../../components/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import ModalConsultation from './ModalConsultation';
import axios from 'axios';
import { useStyles } from './styles';
import { format } from 'date-fns';

function Footer({ open }) {
  const classes = useStyles();

  return (
    <AppBar position="fixed" color="primary" className={classes.appBar}>
      <Toolbar className={classes.footerToolbar}>
        <Button variant="outlined" color="primary" edge="start">
          Ajuda
        </Button>
        <Button variant="contained" color="primary" edge="end" onClick={open}>
          Agendar consulta
        </Button>
      </Toolbar>
    </AppBar>
  );
}

const options = {
  method: 'GET',
  url: 'http://localhost:3333/consultations',
  params: { '': '', _expand: 'patient' },
  headers: {
    Authorization:
      'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiZmFrZSB0b2tlbiJ9.-tvEhfr6_VHfKU9bumcmdvku-IfwZDz2LtjeqZOuH-g',
  },
};

function Home() {
  const classes = useStyles();
  const [consultations, setConsultations] = useState([]);
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    async function getConsultation() {
      const response = await axios.get(
        `${process.env.REACT_APP_BACKEND_URL}/consultations`,
        options,
      );

      setConsultations(response.data);
    }

    getConsultation();
  }, []);

  return (
    <>
      <TopAppBar />
      <Grid container component="main" className={classes.root}>
        <Grid container className={classes.gridSecondary}>
          <div className={classes.heroContent}>
            <Container>
              <Typography
                component="h1"
                variant="h2"
                align="left"
                color="textPrimary"
                gutterBottom
                className={classes.titlePage}
              >
                Consultas
              </Typography>
            </Container>
          </div>

          <Grid container className={classes.gridList}>
            <Grid item xs={12} sm={10} md={6}>
              <Typography
                component="h4"
                color="textPrimary"
                className={classes.listTotal}
                gutterBottom
              >
                {consultations.length}{' '}
                {consultations.length < 2
                  ? 'consulta agendada'
                  : 'consultas agendadas'}
              </Typography>

              <List>
                {consultations.map((consultation) => (
                  <ListItem key={consultation.id}>
                    <ListItemText
                      primary={consultation.patient.first_name}
                      secondary={format(
                        new Date(consultation.date),
                        "dd/MM/yyy' às 'HH:mm",
                      )}
                    />
                    <ListItemAvatar>
                      <Button
                        variant="contained"
                        size="large"
                        color="primary"
                        className={classes.margin}
                      >
                        Atender
                      </Button>
                    </ListItemAvatar>
                  </ListItem>
                ))}
              </List>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Footer open={handleClickOpen} />
      <ModalConsultation open={open} onClose={handleClose} />
    </>
  );
}

export default Home;
