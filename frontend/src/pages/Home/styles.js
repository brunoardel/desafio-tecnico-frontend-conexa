import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: 'calc(100vh - 64px - 64px)',
  },
  gridSecondary: {
    display: 'flex',
    flexDirection: 'column',
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    padding: theme.spacing(6, 0, 4),

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(3, 0),
    },
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    padding: theme.spacing(0, 3, 3, 3),
    display: 'flex',
    justifyContent: 'space-between',
  },
  margin: {
    margin: theme.spacing(1),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  gridList: {
    margin: theme.spacing(8, 0),
    paddingBottom: theme.spacing(4),
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',

    [theme.breakpoints.down('sm')]: {
      margin: theme.spacing(0),
      paddingBottom: theme.spacing(8),
    },
  },
  listTotal: {
    margin: theme.spacing(1, 2),
  },
  appBar: {
    top: 'auto',
    bottom: 0,
    backgroundColor: 'white',
  },
  footerToolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  grow: {
    flexGrow: 1,
  },
  fabButton: {
    position: 'absolute',
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: '0 auto',
  },
  titlePage: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '2rem',
    },
  },
}));
