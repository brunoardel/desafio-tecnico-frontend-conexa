import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import FormInput from '../../components/Input';
import Hidden from '@material-ui/core/Hidden';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import IconVisible from '@material-ui/icons/Visibility';
import IconHidden from '@material-ui/icons/VisibilityOff';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { useAuth } from '../../hooks/auth';
import { useForm, FormProvider } from 'react-hook-form';
import { useStyles } from './styles';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers';
import loginImage from '../../assets/images/login-image.svg';
import AppBar from '../../components/AppBar';

const validationSchema = yup.object().shape({
  email: yup
    .string()
    .required('Insira seu e-mail')
    .email('Insira um e-mail válido'),
  password: yup
    .string()
    .required('Insira sua senha')
    .min(6, 'Senha precisa ter no mínimo 6 caracteres'),
});

function Login(props) {
  const classes = useStyles();
  const { signIn } = useAuth();
  const history = props?.history;
  const [values, setValues] = useState({
    showPassword: false,
  });
  const [requestError, setRequestError] = useState({
    state: false,
    message: '',
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const methods = useForm({
    resolver: yupResolver(validationSchema),
  });

  const { handleSubmit, errors } = methods;

  const onSubmit = async (data) => {
    try {
      await signIn({ ...data });

      history.push('/');
    } catch (error) {
      if (error?.response?.status === 401) {
        setRequestError({
          state: true,
          message: 'E-mail ou senha incorretos',
        });
      }
    }
  };

  return (
    <>
      <AppBar />
      <Grid container component="main" className={classes.root}>
        <Grid container>
          <Grid item xs={12} sm={4} md={6} className={classes.gridImage}>
            <Typography
              variant="h2"
              component="h1"
              className={classes.gridImageTitle}
            >
              Faça Login
            </Typography>
            <Hidden smDown>
              <img
                src={loginImage}
                alt="imagem caderno mesa"
                className={classes.loginImage}
              />
            </Hidden>
          </Grid>

          <Grid item xs={12} sm={8} md={6} className={classes.gridForm}>
            <FormProvider {...methods}>
              <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
                {requestError.state && (
                  <Typography
                    variant="h6"
                    color="error"
                    className={classes.requestErrorNotification}
                  >
                    <ErrorOutlineIcon />
                    E-mail ou senha incorreto
                  </Typography>
                )}
                <FormInput
                  name="email"
                  label="E-mail"
                  required={true}
                  errorobj={errors}
                  variant="standard"
                  margin="normal"
                  fullWidth
                  id="email"
                  placeholder="Digite seu e-mail"
                  autoComplete="email"
                  InputLabelProps={{ shrink: true }}
                  className={classes.formInput}
                />
                <FormInput
                  name="password"
                  label="Senha"
                  required={true}
                  errorobj={errors}
                  variant="standard"
                  margin="normal"
                  fullWidth
                  placeholder="Digite sua senha"
                  id="password"
                  autoComplete="current-password"
                  InputLabelProps={{ shrink: true }}
                  type={values.showPassword ? 'text' : 'password'}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {values.showPassword ? (
                            <IconVisible />
                          ) : (
                            <IconHidden />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />

                <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  type="submit"
                >
                  Entrar
                </Button>
              </form>
            </FormProvider>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}

export default Login;
