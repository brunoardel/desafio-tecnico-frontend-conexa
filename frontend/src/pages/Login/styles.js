import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme) => ({
  root: {
    height: 'calc(100vh - 64px)',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      alignContent: 'center',
      justifyContent: 'space-evenly',
    },
  },
  paper: {
    margin: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  gridForm: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  form: {
    maxWidth: 300,
    margin: 'auto',
  },
  formInput: {
    margin: theme.spacing(0, 0, 3, 0),
  },
  submit: {
    margin: theme.spacing(8, 0, 0, 0),
  },
  gridImage: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  gridImageTitle: {
    margin: theme.spacing(0, 0, 8, 0),

    [theme.breakpoints.down('sm')]: {
      fontSize: '2.5rem',
    },
  },
  requestErrorNotification: {
    display: 'flex',
    alignItems: 'center',
    margin: theme.spacing(0, 0, 4, 0),

    '& > svg': {
      margin: theme.spacing(0, 1),
    },
  },
}));
